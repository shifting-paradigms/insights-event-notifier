# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.1] - 2023-03-20
### Added
- Send notification if error occurs
### Fixed
- Send summary notification only if critical events were found

## [1.1.0] - 2023-03-16
### Added
- Support tls encryption for websocket

## [1.0.0] - 2023-02-27
### Added
- Initial release
- Support for [insights-server v1.8.3](https://gitlab.com/shifting-paradigms/insights-server/-/tree/1.8.3)
