# Insights Event Notifier


The purpose of this tool is to notify the user about critical events on a system observed by [insights-server](https://gitlab.com/shifting-paradigms/insights-server).

On execution, the tool will connect to the insights-server and fetch all log events of the last `SCAN_HOURS` hours. It then scans the received log events and notifies the user in case of critical events.

The tool is intended to be run periodically, e.g. via cron.


## Usage
```bash
# run locally
SCAN_HOURS=1 INSIGHTS_URL="wss://insights.shifting-paradigms.org/insights" AUTH_TOKEN=<bearer-token> cargo run

# install binary
cargo install --path .
```
