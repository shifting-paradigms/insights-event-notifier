use futures_util::{SinkExt, StreamExt};
use http::HeaderValue;
use insights_event_notifier::{
    send_error_message, send_report, send_summary, Config, Log, MetricsPackage, StreamConfig,
};
use native_tls::TlsConnector;
use secrecy::ExposeSecret;
use std::time::Duration;
use tokio_tungstenite::{
    connect_async_tls_with_config,
    tungstenite::{client::IntoClientRequest, Message},
    Connector,
};

#[tokio::main]
async fn main() {
    env_logger::init_from_env(env_logger::Env::default().default_filter_or("info"));

    let config = match Config::try_from_env() {
        Ok(config) => {
            log::debug!("Config: {:#?}", config);
            config
        }
        Err(e) => {
            let error_msg = format!("Failed to read config from environment variables: {}", e);
            log::error!("{}", &error_msg);
            send_error_message(&error_msg);
            std::process::exit(1);
        }
    };
    let stream_config = StreamConfig::from(&config);

    let mut request = if let Ok(request) = config.insights_url.into_client_request() {
        request
    } else {
        let error_msg = "Failed to parse insights url";
        log::error!("{}", error_msg);
        send_error_message(error_msg);
        std::process::exit(1);
    };

    let headers = request.headers_mut();
    let header_value: HeaderValue =
        if let Ok(value) = format!("Bearer {}", config.auth_token.expose_secret()).parse() {
            value
        } else {
            let error_msg = "Failed to parse auth token";
            log::error!("{}", error_msg);
            send_error_message(error_msg);
            std::process::exit(1);
        };
    headers.insert("Authorization", header_value);

    let tls_connector = match TlsConnector::new() {
        Ok(connector) => connector,
        Err(e) => {
            let error_msg = format!("Failed to create tls connector: {}", e);
            log::error!("{}", &error_msg);
            send_error_message(&error_msg);
            std::process::exit(1);
        }
    };

    let (socket, response) = match connect_async_tls_with_config(
        request,
        None,
        Some(Connector::NativeTls(tls_connector)),
    )
    .await
    {
        Ok(socket_response) => socket_response,
        Err(e) => {
            let error_msg = format!("Failed to connect to insights server: {}", e);
            log::error!("{}", &error_msg);
            send_error_message(&error_msg);
            std::process::exit(1);
        }
    };

    log::info!("Connected to insights server");
    log::debug!("Response HTTP code: {}", response.status());
    log::debug!(
        "Response contains the following headers: {:#?}",
        response.headers()
    );

    let (mut write, mut read) = socket.split();

    let config_json = if let Ok(parsed) = serde_json::to_string(&stream_config) {
        parsed
    } else {
        let error_msg = "Failed to parse stream config to json string";
        log::error!("{}", error_msg);
        send_error_message(error_msg);
        std::process::exit(1);
    };

    if write.send(Message::Text(config_json)).await.is_ok() {
        log::debug!("Sent stream config to insights server");
    } else {
        let error_msg = "Failed to send stream config to insights server";
        log::error!("{}", error_msg);
        send_error_message(error_msg);
        std::process::exit(1);
    }

    let mut logs: Vec<Log> = Vec::with_capacity(10_000);

    log::info!("Listening for logs");
    let mut interval = tokio::time::interval(Duration::from_secs(5));
    interval.reset();
    loop {
        tokio::select! {
            _ = interval.tick() => {
                log::debug!("Timeout reached, waiting for new logs");
                break;
            },
            Some(message) = read.next() => {
                log::trace!("Received message from insights server");
                match message {
                    Ok(message) => match message.to_text() {
                        Ok(payload) => {
                            let package: MetricsPackage<Log> = if let Ok(parsed) = serde_json::from_str(payload) {
                                parsed
                            } else {
                                let error_msg = format!("Failed to parse metrics package: {}", payload);
                                log::error!("{}", &error_msg);
                                send_error_message(&error_msg);
                                continue;
                            };
                            logs.push(package.data);
                            interval.reset();
                        }
                        Err(e) => {
                            let error_msg = format!("Failed to parse message to text: {}", e);
                            log::error!("{}", &error_msg);
                            send_error_message(&error_msg);
                        }
                    },
                    Err(e) => {
                        let error_msg = format!("Failed to read message: {}", e);
                        log::error!("{}", &error_msg);
                        send_error_message(&error_msg);
                    }
                }
            }
        }
    }

    log::info!("Received {} logs", logs.len());

    logs.iter().for_each(|log| match log.level.as_str() {
        "ERROR" => {
            log::debug!("Sending report for log: {:#?}", log);
            send_report(log);
        }
        "WARN" => {
            log::debug!("Sending report for log: {:#?}", log);
            send_report(log);
        }
        _ => {}
    });

    if !logs.is_empty() {
        send_summary(&logs);
    }

    log::info!("Finished");
}
