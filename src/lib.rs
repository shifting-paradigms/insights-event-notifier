use notify_rust::{Notification, Timeout};
use secrecy::SecretString;
use serde::{Deserialize, Serialize};
use std::{
    env,
    fmt::{Display, Formatter},
    time::Duration,
};
use time::OffsetDateTime;

#[derive(Clone, Debug)]
pub struct Config {
    scan_hours: u64,
    pub insights_url: String,
    pub auth_token: SecretString,
}

impl Config {
    pub fn try_from_env() -> anyhow::Result<Self> {
        log::debug!("Reading config from environment variables");

        let scan_hours = match env::var("SCAN_HOURS") {
            Ok(val) => {
                if val.parse::<u64>().is_err() {
                    anyhow::bail!("SCAN_HOURS is not a valid number");
                }
                val.parse::<u64>()?
            }
            Err(_) => {
                let default = 24;
                log::warn!("SCAN_HOURS not set, using default value: {}", default);
                default
            }
        };
        let insights_url = match env::var("INSIGHTS_URL") {
            Ok(val) => val,
            Err(_) => {
                anyhow::bail!("INSIGHTS_URL not set");
            }
        };
        let auth_token = match env::var("AUTH_TOKEN") {
            Ok(val) => val,
            Err(_) => {
                anyhow::bail!("AUTH_TOKEN not set");
            }
        };

        Ok(Self {
            scan_hours,
            insights_url,
            auth_token: SecretString::new(auth_token),
        })
    }
}

#[derive(Debug, Serialize)]
pub struct StreamConfig {
    log: LogConfig,
}

#[derive(Debug, Serialize)]
struct LogConfig {
    interval: u64,
    #[serde(with = "time::serde::rfc3339")]
    start_timestamp: OffsetDateTime,
}

impl From<&Config> for StreamConfig {
    fn from(config: &Config) -> Self {
        let duration = Duration::from_secs(config.scan_hours * 60 * 60);
        Self {
            log: LogConfig {
                interval: 1,
                start_timestamp: OffsetDateTime::now_utc() - duration,
            },
        }
    }
}

/// A package of metrics.
///
/// This is used to send metrics into the websocket stream.
#[derive(Debug, Deserialize, Serialize)]
pub struct MetricsPackage<T> {
    pub id: String,
    pub kind: MetricsType,
    pub data: T,
}

/// The type of metrics.
///
/// This is used to determine what type of metrics are being sent into the websocket stream.
#[derive(Debug, Deserialize, Serialize)]
pub enum MetricsType {
    ApiMetrics,
    HostMetrics,
    LogEntry,
}

/// The log metrics type.
///
/// This type is used to store the log entries in the database
/// and to send them into the stream.
#[derive(Deserialize, Debug, Clone, Serialize)]
pub struct Log {
    /// The timestamp of the log entry.
    pub timestamp: String,
    /// The level of the log entry.
    pub level: String,
    /// The message of the log entry.
    pub message: String,
    /// The target of the log entry.
    pub target: Option<String>,
    /// The span of the log entry.
    pub span: Option<serde_json::Value>,
    /// The time when the log entry was stored in the database.
    ///
    /// This field is used to sort the log entries.
    /// It is also used to determine if the log entry is new
    /// or if it was already sent to the stream.
    #[serde(with = "time::serde::rfc3339")]
    pub created_at: OffsetDateTime,
}

struct LogReport {
    level: String,
    message: String,
    target: Option<String>,
    timestamp: String,
}

impl From<&Log> for LogReport {
    fn from(log: &Log) -> Self {
        Self {
            level: log.level.to_string(),
            message: log.message.to_string(),
            target: log.target.clone(),
            timestamp: log.timestamp.to_string(),
        }
    }
}

impl Display for LogReport {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "\n{}: {}\n\nat: {}\nin: {}\n\n",
            self.level,
            self.message,
            self.timestamp,
            self.target.as_deref().unwrap_or("unknown target")
        )
    }
}

pub fn send_report(log: &Log) {
    let report = LogReport::from(log).to_string();
    if Notification::new()
        .summary("Important Event Detected!")
        .body(&report)
        .timeout(Timeout::from(Duration::from_secs(10)))
        .show()
        .is_err()
    {
        log::error!("Failed to send notification");
    }
}

pub fn send_summary(logs: &[Log]) {
    log::info!("Sending summary notification");
    let mut error_count = 0;
    let mut warning_count = 0;

    log::debug!("Counting log events");
    for log in logs {
        match log.level.as_str() {
            "ERROR" => error_count += 1,
            "WARN" => warning_count += 1,
            _ => (),
        }
    }

    let message = format!(
        "{} errors detected\n{} warnings detected\n",
        error_count, warning_count
    );

    log::debug!("Sending notification");
    if Notification::new()
        .summary("Important Events Detected!")
        .body(&message)
        .timeout(Timeout::Never)
        .show()
        .is_err()
    {
        log::error!("Failed to send notification");
    }
}

pub fn send_error_message(message: &str) {
    log::error!("Sending error notification");
    if Notification::new()
        .summary("Error!")
        .body(message)
        .timeout(Timeout::from(Duration::from_secs(10)))
        .show()
        .is_err()
    {
        log::error!("Failed to send notification");
    }
}
